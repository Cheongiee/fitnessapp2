import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  Dimensions,
  Image,
  TouchableWithoutFeedback,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
// import workoutImages from '../../assets/images/index/workoutsIndex';
import IonIcon from 'react-native-vector-icons/Ionicons';
// import { TouchableOpacity } from 'react-native-gesture-handler';
import NavigationService from '../../navigation/services/NavigationService';

import AtributeItem from './AtributeItem';

const {width, height} = Dimensions.get('screen');

const CircuitItem = props => {
  if (!props.scrollLayout) {
    return (
      <View>
        <AtributeItem attribute={props.attribute} />
        <FlatList
          contentContainerStyle={{
            paddingVertical: 10,
          }}
          scrollEnabled={false}
          data={props.data}
          keyExtractor={item => item.id}
          renderItem={itemData => (
            <View>
              {/* <TouchableWithoutFeedback
                onPress={() => {
                  selectItemHandler(itemData.item.id, itemData.item.title);
                }}
                useForeground> */}
              <View style={[{flex: 1, flexDirection: 'row'}]}>
                <View style={(styles.container, styles.workoutsSecond)}>
                  <View style={styles.container}>
                    <View style={[styles.headerContainer, {flex: 1}]}>
                      <View
                        style={{
                          flex: 1,
                          justifyContent: 'center',
                          paddingLeft: 20,
                        }}>
                        <Text
                          style={{
                            color: '#2FBF71',
                            fontSize: 17,
                            fontWeight: 'bold',
                          }}>
                          {itemData.item.title}
                        </Text>
                        <View style={styles.headerDetailContainer}>
                          <Text style={styles.workoutDetailText}>
                            Recommended:{' '}
                          </Text>
                          <Text style={styles.workoutDetailText}>
                            {itemData.item.time}
                          </Text>
                        </View>
                      </View>
                      <FontAwesome5
                        style={{paddingRight: 10}}
                        name="chevron-right"
                        size={12}
                        color="#2FBF71"
                      />
                    </View>
                  </View>
                  <View
                    style={{
                      height: 3,
                      backgroundColor: '#2FBF71',
                      borderBottomLeftRadius: 5,
                      borderBottomRightRadius: 5,
                    }}
                  />
                </View>
              </View>
              {/* </TouchableWithoutFeedback> */}
            </View>
          )}
        />
      </View>
    );
  }
  return (
    <View>
      <AtributeItem attribute={props.attribute} />
      <FlatList
        contentContainerStyle={{
          paddingRight: 20,
        }}
        horizontal
        scrollEnabled
        showsHorizontalScrollIndicator={false}
        scrollEventThrottle={16}
        data={props.data}
        keyExtractor={item => item.id}
        renderItem={itemData => (
          <View style={(styles.container, styles.workouts)}>
            <TouchableWithoutFeedback
              onPress={() => {
                props.onSelect(
                  itemData.item.workouts,
                  itemData.item.imageUrl,
                  itemData.item.title,
                  itemData.item.sequences,
                  itemData.item.exercises,
                  itemData.item.time,
                );
              }}
              useForeground>
              <View style={{height: '100%', width: '100%'}}>
                <View style={styles.imageViewContiner}>
                  <Image
                    style={styles.imageContainer}
                    source={{uri: itemData.item.imageUrl}}
                  />
                </View>
                <View style={styles.container}>
                  <View
                    style={{
                      flex: 1,
                      justifyContent: 'center',
                      paddingLeft: 20,
                    }}>
                    <Text
                      style={{
                        color: '#FF1654',
                        fontSize: 17,
                        fontWeight: 'bold',
                      }}>
                      {itemData.item.title}
                    </Text>
                    <View style={styles.headerDetailContainer}>
                      <Text style={styles.workoutDetailText}>
                        {itemData.item.sequences} Sequences
                      </Text>
                      <Text style={styles.workoutDetailText}> - </Text>
                      <Text style={styles.workoutDetailText}>
                        {itemData.item.exercises}
                      </Text>
                      <Text style={styles.workoutDetailText}> - </Text>
                      <Text style={styles.workoutDetailText}>
                        {itemData.item.time}
                      </Text>
                    </View>
                  </View>
                </View>
              </View>
            </TouchableWithoutFeedback>
          </View>
        )}
      />
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  headerText: {
    fontSize: 25,
    fontWeight: 'bold',
  },
  headerDetailContainer: {
    paddingTop: 3,
    flexDirection: 'row',
  },
  headerDetailText: {
    fontSize: 12,
    fontWeight: 'bold',
    color: 'gray',
  },
  workoutDetailText: {
    fontSize: 12,
    color: 'gray',
  },
  workouts: {
    width: width - 36 * 3,
    height: width - 36 * 4,
    marginLeft: 20,
    marginVertical: 20,
    borderRadius: 5,
    backgroundColor: 'white',
    borderColor: 'lightgray',
    borderWidth: 0.5,
    shadowOpacity: 0.2,
    shadowRadius: 10,
    shadowColor: 'black',
    shadowOffset: {height: 3, width: 0},
    elevation: 10,
  },
  workoutsSecond: {
    flex: 1,
    alignItems: 'stretch',
    height: 80,
    marginHorizontal: 20,
    marginVertical: 8,
    borderRadius: 5,
    backgroundColor: 'white',
    borderColor: 'lightgray',
    borderWidth: 0.5,
    shadowOpacity: 0.2,
    shadowRadius: 10,
    shadowColor: 'black',
    shadowOffset: {height: 3, width: 0},
    elevation: 10,
  },
  imageViewContiner: {
    height: '70%',
    overflow: 'hidden',
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
  },
  imageContainer: {
    width: '100%',
    height: '100%',
  },
});

export default CircuitItem;
