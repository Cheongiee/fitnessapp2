import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  TouchableOpacity,
  TouchableNativeFeedback,
  Platform,
} from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Colors from '../../constants/Colors';

const ProgramTitle = props => {
  return (
    <View style={{padding: 20}}>
      <View style={styles.headerContainer}>
        <Text style={styles.headerText}>MY PROGRAM</Text>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <FontAwesome5
            name="calendar-check"
            containerStyle={{marginTop: 6}}
            size={20}
            color={'#FF1654'}
          />
          <Text style={{paddingLeft: 5, paddingTop: 3, color: '#FF1654'}}>
            Planner
          </Text>
        </View>
      </View>
      <View style={styles.headerDetailContainer}>
        <Text style={styles.headerDetailText}>Week {props.week}</Text>
        <Text style={styles.headerDetailText}> - </Text>
        <Text style={styles.headerDetailText}>{props.title}</Text>
        <Text style={styles.headerDetailText}> - </Text>
        <Text style={styles.headerDetailText}>{props.instructor}</Text>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  centered: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    flex: 1,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  headerText: {
    fontSize: 25,
    fontWeight: 'bold',
  },
  headerDetailContainer: {
    paddingTop: 3,
    flexDirection: 'row',
  },
  headerDetailText: {
    fontSize: 12,
    fontWeight: 'bold',
    color: 'gray',
  },
});

export default ProgramTitle;
