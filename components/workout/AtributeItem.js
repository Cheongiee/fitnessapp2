import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  Dimensions,
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
} from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
// import workoutImages from '../../assets/images/index/workoutsIndex';
import IonIcon from 'react-native-vector-icons/Ionicons';

const {width, height} = Dimensions.get('screen');

const AtributeItem = props => {
  return (
    <View style={styles.headerContainer}>
      <View
        style={{
          flex: 1,
          flexDirection: 'row',
          paddingLeft: 20,
          alignItems: 'center',
        }}>
        <Text style={[styles.headerText, {fontSize: 19}]}>
          {props.attribute}
        </Text>
        <IonIcon
          style={{paddingLeft: 10}}
          name="ios-information-circle-outline"
          size={20}
        />
      </View>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          paddingRight: 10,
        }}>
        <FontAwesome5 style={{paddingRight: 10}} name="trophy" size={15} />
        <FontAwesome5 style={{paddingRight: 10}} name="trophy" size={15} />
        <FontAwesome5 style={{paddingRight: 10}} name="trophy" size={15} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  headerText: {
    fontSize: 25,
    fontWeight: 'bold',
  },
  headerDetailContainer: {
    paddingTop: 3,
    flexDirection: 'row',
  },
  headerDetailText: {
    fontSize: 12,
    fontWeight: 'bold',
    color: 'gray',
  },
});

export default AtributeItem;
