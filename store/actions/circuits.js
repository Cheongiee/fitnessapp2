import Circuit from '../../models/circuit';

export const SET_CIRCUITS = 'SET_CIRCUITS';

// export const fetchPrograms = () => {
export const fetchCircuits = () => {
  return async dispatch => {
    try {
      const response = await fetch(
        'https://fitnessapp2-ed91e.firebaseio.com/circuits.json',
      );
      if (!response.ok) {
        throw new Error('Something went wrong!');
      }

      const resData = await response.json();
      const loadedCircuits = [];

      for (const key in resData) {
        loadedCircuits.push(
          new Circuit(
            key,
            resData[key].title,
            resData[key].attribute,
            resData[key].exercises,
            resData[key].imageUrl,
            resData[key].description,
            resData[key].time,
            resData[key].sequences,
            resData[key].workouts,
            resData[key].workoutsName,
          ),
        );
      }
      dispatch({type: SET_CIRCUITS, circuits: loadedCircuits});
    } catch (err) {
      //send custom analytics
      throw err;
    }
  };
};
