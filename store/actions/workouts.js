import Workout from '../../models/workout';

export const SET_WORKOUTS = 'SET_WORKOUTS';

// export const fetchPrograms = () => {
export const fetchWorkouts = () => {
  return async dispatch => {
    try {
      const response = await fetch(
        'https://fitnessapp2-ed91e.firebaseio.com/workouts.json',
      );
      if (!response.ok) {
        throw new Error('Something went wrong!');
      }

      const resData = await response.json();
      const loadedWorkouts = [];

      for (const key in resData) {
        loadedWorkouts.push(
          new Workout(
            key,
            resData[key].title,
            resData[key].program,
            resData[key].circuit,
            resData[key].imageUrl,
            resData[key].videoUrl,
            resData[key].description,
            resData[key].instructions,
            resData[key].time,
            resData[key].laps,
            resData[key].color,
          ),
        );
      }
      dispatch({type: SET_WORKOUTS, workouts: loadedWorkouts});
    } catch (err) {
      //send custom analytics
      throw err;
    }
  };
};
