import {SET_WORKOUTS} from '../actions/workouts';

const intialState = {
  availableWorkouts: [],
};

export default (state = intialState, action) => {
  switch (action.type) {
    case SET_WORKOUTS:
      return {
        availableWorkouts: action.workouts,
      };
  }
  return state;
};
