import {SET_CIRCUITS} from '../actions/circuits';

const intialState = {
  availableCircuits: [],
};

export default (state = intialState, action) => {
  switch (action.type) {
    case SET_CIRCUITS:
      return {
        availableCircuits: action.circuits,
      };
  }
  return state;
};
