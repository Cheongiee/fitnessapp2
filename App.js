import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
// import Icon from 'react-native-vector-icons/FontAwesome';
import {createStore, combineReducers, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import ReduxThunk from 'redux-thunk';
import {createAppContainer} from 'react-navigation';
import NavigationService from './navigation/services/NavigationService';

import workoutsReducer from './store/reducers/workouts';
import circuitsReducer from './store/reducers/circuits';
import OverviewNav from './navigation/OverviewNav';

const rootReducer = combineReducers({
  workouts: workoutsReducer,
  circuits: circuitsReducer,
});

//Devtool to track redux state
// const store = createStore(rootReducer, composeWithDevTools());

const AppContainer = createAppContainer(OverviewNav);

const store = createStore(rootReducer, applyMiddleware(ReduxThunk));
const App = () => {
  return (
    <Provider store={store}>
      <AppContainer
        ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}
      />
      {/* <OverviewNav /> */}
    </Provider>
  );
};

export default App;
