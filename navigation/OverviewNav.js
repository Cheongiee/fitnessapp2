import React from 'react';
// import {SafeAreaView, StyleSheet, ScrollView, View, Text} from 'react-native';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {createDrawerNavigator} from 'react-navigation-drawer';
import {Platform} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import WorkoutsOverviewScreen from '../screens/workouts/WorkoutsOverviewScreen';
import PlannerOverviewScreen from '../screens/planner/PlannerOverviewScreen';
import WorkoutDetailScreen from '../screens/workouts/WorkoutDetailScreen';
import Colors from '../constants/Colors';

const defaultNavOptions = {
  headerStyle: {
    backgroundColor: Platform.OS === 'android' ? Colors.primary : '',
  },
  headerTintColor: Platform.OS === 'android' ? 'white' : Colors.primary,
};

const ModalStacks = createStackNavigator(
  {
    WorkoutDetailModal: WorkoutDetailScreen,
  },
  {
    navigationOptions: {
      gesturesEnabled: false,
    },
  },
);
const PlannerNavigator = createStackNavigator(
  {
    PlannerOverview: PlannerOverviewScreen,
    // WorkoutDetail: WorkoutDetailScreen,
  },
  {
    navigationOptions: {
      tabBarLabel: 'Planner',
      tabBarIcon: ({tintColor}) => (
        <FontAwesome5
          name="dragon"
          containerStyle={{marginTop: 6}}
          size={24}
          color={tintColor}
        />
      ),
    },
    defaultNavigationOptions: defaultNavOptions,
  },
);

const WorkoutsNavigator = createStackNavigator(
  {
    WorkoutsOverview: WorkoutsOverviewScreen,
    // WorkoutDetail: WorkoutDetailScreen,
  },
  {
    navigationOptions: {
      tabBarLabel: 'Workouts',
      tabBarIcon: ({tintColor}) => (
        <FontAwesome5
          name="running"
          containerStyle={{marginTop: 6}}
          size={24}
          color={tintColor}
        />
      ),
    },
    defaultNavigationOptions: defaultNavOptions,
  },
);

const OverviewNav = createBottomTabNavigator(
  {
    Workouts: WorkoutsNavigator,
    Food: WorkoutsNavigator,
    Planner: PlannerNavigator,
    Progress: WorkoutsNavigator,
    Community: WorkoutsNavigator,
  },
  {
    tabBarOptions: {
      tabStyle: {
        paddingTop: 5,
      },
      activeTintColor: Colors.primary,
      inactiveTintColor: Colors.inactive,
    },
  },
);

const DashboardStackNavigator = createStackNavigator(
  {
    DashboardTabNavigator: OverviewNav,
    ModalStackNav: ModalStacks,
  },
  {
    headerMode: 'none',
    mode: 'modal',
    navigationOptions: ({navigation}) => {
      return {
        header: null,
      };
    },
  },
);

const AppTabNavigator = createDrawerNavigator(
  {
    Dashboard: {screen: DashboardStackNavigator},
  },
  // {
  //   contentComponent: SideDrawer,
  // },
);

const AppSwitchNavigator = createSwitchNavigator({
  // Auth: {
  //   screen: AuthScreen,
  // },
  Dashboard: {
    screen: AppTabNavigator,
  },
});

export default createAppContainer(AppSwitchNavigator);
