export default {
  primary: '#FF1654',
  accent: '#FFC107',
  inactive: 'gray',
};
