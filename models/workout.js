//OPTIONAL
class Workout {
  constructor(
    id,
    title,
    program,
    circuits,
    imageUrl,
    videoUrl,
    description,
    instructions,
    time,
    laps,
    color,
  ) {
    this.id = id;
    this.title = title;
    this.program = program;
    this.circuits = circuits;
    this.imageUrl = imageUrl;
    this.videoUrl = videoUrl;
    this.description = description;
    this.instructions = instructions;
    this.time = time;
    this.laps = laps;
    this.color = color;
  }
}

export default Workout;
