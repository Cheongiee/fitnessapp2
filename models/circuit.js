//OPTIONAL
class Circuit {
  constructor(
    id,
    title,
    attribute,
    exercises,
    imageUrl,
    description,
    time,
    sequences,
    workouts,
    workoutsName,
  ) {
    this.id = id;
    this.title = title;
    this.attribute = attribute;
    this.exercises = exercises;
    this.imageUrl = imageUrl;
    this.description = description;
    this.time = time;
    this.sequences = sequences;
    this.workouts = workouts;
    this.workoutsName = workoutsName;
  }
}

export default Circuit;
