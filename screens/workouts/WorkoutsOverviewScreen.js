import React, {useState, useEffect, useCallback} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  Platform,
  ActivityIndicator,
  Button,
  YellowBox,
  TouchableHighlight,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {HeaderButtons, Item} from 'react-navigation-header-buttons';
import {useSelector, useDispatch} from 'react-redux';

import HeaderButton from '../../components/UI/HeaderButton';
import ProgramTitle from '../../components/workout/ProgramTitle';
import AtributeItem from '../../components/workout/AtributeItem';
import CircuitItem from '../../components/workout/CircuitItem';
import * as workoutActions from '../../store/actions/workouts';
import * as circuitActions from '../../store/actions/circuits';
import Colors from '../../constants/Colors';

YellowBox.ignoreWarnings([
  'VirtualizedLists should never be nested', // TODO: Remove when fixed
]);

const WorkoutsOverviewScreen = props => {
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState();
  const workouts = useSelector(state => state.workouts.availableWorkouts);
  const circuits = useSelector(state => state.circuits.availableCircuits);

  const dispatch = useDispatch();

  const loadCircuits = useCallback(async () => {
    setError(null);
    try {
      await dispatch(circuitActions.fetchCircuits());
    } catch (err) {
      setError(err.message);
    }
  }, [dispatch, setIsLoading, setError]);

  useEffect(() => {
    const willFocusSub = props.navigation.addListener(
      'willFocus',
      loadCircuits,
    );

    return () => {
      willFocusSub.remove();
    };
  }, [loadCircuits]);

  useEffect(() => {
    setIsLoading(true);
    loadCircuits().then(() => {
      setIsLoading(false);
    });
  }, [dispatch, loadCircuits]);

  if (error) {
    return (
      <View>
        <View style={styles.centered}>
          <Text>An error occured!</Text>
          <Button
            title="Try again"
            onPress={loadCircuits}
            color={Colors.primary}
          />
        </View>
      </View>
    );
  }

  if (isLoading) {
    return (
      <View style={styles.centered}>
        <ActivityIndicator size="large" color={Colors.primary} />
      </View>
    );
  }

  if (!isLoading && circuits.length === 0) {
    return (
      <View style={styles.centered}>
        <Text>No products found. Maybe start adding some!</Text>
      </View>
    );
  }

  const selectItemHandler = (
    id,
    imageUrl,
    title,
    sequences,
    exercises,
    time,
  ) => {
    props.navigation.navigate('WorkoutDetailModal', {
      workoutId: id,
      headerImage: imageUrl,
      title: title,
      sequences: sequences,
      exercises: exercises,
      time: time,
    });
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      <ScrollView>
        <ProgramTitle week="9" title="BAM 1.0" instructor="Sjana Elise" />
        <CircuitItem
          attribute="Resistance"
          data={circuits.filter(circ => circ.attribute === 'Resistance')}
          scrollLayout={true}
          onSelect={selectItemHandler}
        />
        <CircuitItem
          attribute="Cardio"
          data={circuits.filter(circ => circ.attribute === 'Cardio')}
          scrollLayout={false}
          // onSelect={selectItemHandler}
        />
        <CircuitItem
          attribute="Recovery"
          data={circuits.filter(circ => circ.attribute === 'Recovery')}
          scrollLayout={true}
          onSelect={selectItemHandler}
        />
      </ScrollView>
    </SafeAreaView>
  );
};

WorkoutsOverviewScreen.navigationOptions = navData => {
  return {
    headerTitle: 'FITNESS APP',
    headerLeft: (
      <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item
          title="Menu"
          iconName={Platform.OS === 'android' ? 'bars' : 'bars'}
          // onPress={() => {
          // navData.navigation.toggleDrawer();
          // }}
        />
      </HeaderButtons>
    ),
    headerRight: (
      <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item
          title="Cart"
          iconName={
            Platform.OS === 'android' ? 'shopping-cart' : 'shopping-cart'
          }
          // onPress={() => {
          // navData.navigation.toggleDrawer();
          // }}
        />
      </HeaderButtons>
    ),
  };
};

const styles = StyleSheet.create({
  centered: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default WorkoutsOverviewScreen;
