import React, {useEffect, useState} from 'react';
import {
  ScrollView,
  View,
  Text,
  Image,
  Button,
  StyleSheet,
  TouchableOpacity,
  FlatList,
  Dimensions,
  Animated,
  ActivityIndicator,
} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import Colors from '../../constants/Colors';
import * as workoutActions from '../../store/actions/workouts';
import * as circuitActions from '../../store/actions/circuits';
import NavigationService from '../../navigation/services/NavigationService';
import ThemeUtils from '../../utils/ThemeUtils';

const {width, height} = Dimensions.get('screen');

const WorkoutDetailScreen = props => {
  this.state = {
    scrollY: new Animated.Value(0),
  };

  _getHeaderImageOpacity = () => {
    const {scrollY} = this.state;

    return scrollY.interpolate({
      inputRange: [0, 140],
      outputRange: [1, 0],
      extrapolate: 'clamp',
      useNativeDriver: true,
    });
  };

  _getHeaderTopPosition = () => {
    const {scrollY} = this.state;

    return scrollY.interpolate({
      inputRange: [0, 100],
      outputRange: [
        0,
        -(ThemeUtils.relativeWidth(100) - ThemeUtils.APPBAR_HEIGHT),
      ],
      extrapolate: 'clamp',
      useNativeDriver: true,
    });
  };

  _getHeaderScale = () => {
    const {scrollY} = this.state;

    return scrollY.interpolate({
      inputRange: [-300, 0],
      outputRange: [600, 300],
      extrapolate: 'clamp',
      useNativeDriver: true,
    });
  };

  _getListViewTopPosition = () => {
    const {scrollY} = this.state;

    return scrollY.interpolate({
      inputRange: [0, 300],
      outputRange: [0, -300],
      extrapolate: 'clamp',
      useNativeDriver: true,
    });
  };

  _getHeaderTitleOpacity = () => {
    const {scrollY} = this.state;

    return scrollY.interpolate({
      inputRange: [0, 20, 50],
      outputRange: [0, 0.5, 1],
      extrapolate: 'clamp',
      useNativeDriver: true,
    });
  };

  const [isLoading, setIsLoading] = useState(false);
  const workoutId = props.navigation.getParam('workoutId');
  const headerImage = props.navigation.getParam('headerImage');
  const workoutTitle = props.navigation.getParam('title');
  const workoutSequences = props.navigation.getParam('sequences');
  const workoutExercises = props.navigation.getParam('exercises');
  const workoutITime = props.navigation.getParam('time');
  const workoutItems = useSelector(state =>
    state.workouts.availableWorkouts.filter(
      value => -1 !== workoutId.indexOf(value.id),
    ),
  );

  const dispatch = useDispatch();

  useEffect(() => {
    setIsLoading(true);
    dispatch(workoutActions.fetchWorkouts()).then(() => {
      setIsLoading(false);
    });
  }, [dispatch]);

  if (isLoading) {
    return (
      <View style={styles.centered}>
        <ActivityIndicator size="large" color={Colors.primary} />
      </View>
    );
  }

  const listViewTop = this._getListViewTopPosition();
  const headerTitleOpacity = this._getHeaderTitleOpacity();
  const headerScale = this._getHeaderScale();

  return (
    <View style={styles.container}>
      <Animated.View
        style={[
          styles.topBarContainer,
          {
            opacity: headerTitleOpacity,
            backgroundColor: 'white',
            borderBottomColor: '#bbb',
            borderBottomWidth: StyleSheet.hairlineWidth,
          },
        ]}></Animated.View>
      <View style={styles.topBarContainer}>
        <View
          style={{paddingTop: 35, paddingLeft: 5, alignItems: 'flex-start'}}>
          <TouchableOpacity
            onPress={() => NavigationService.navigate('Workouts')}>
            <View style={{padding: 10}}>
              <Text
                style={{color: '#FF1654', fontWeight: 'bold', fontSize: 18}}>
                {'<'} Back
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
      <Animated.View
        style={{
          transform: [
            {
              translateY: listViewTop,
            },
          ],
        }}>
        <Animated.Image
          style={[
            styles.headerImageStyle,
            {
              height: headerScale,
            },
          ]}
          source={{uri: headerImage}}
        />
      </Animated.View>
      <Animated.ScrollView
        style={{zIndex: 10}}
        scrollEventThrottle={16}
        onScroll={Animated.event([
          {
            nativeEvent: {contentOffset: {y: this.state.scrollY}},
          },
        ])}>
        <View style={{paddingTop: 300}}>
          <View style={{padding: 20}}>
            <View style={{margin: 1}}>
              <Text>Week 5 - BAM 1.0</Text>
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                }}>
                <Text style={styles.headerText}>{workoutTitle}</Text>
                <View style={styles.headerDetailContainer}>
                  <Text style={styles.workoutDetailText}>
                    {workoutSequences} Sequences
                  </Text>
                  <Text style={styles.workoutDetailText}> - </Text>
                  <Text style={styles.workoutDetailText}>
                    {workoutExercises} Poses
                  </Text>
                  <Text style={styles.workoutDetailText}> - </Text>
                  <Text style={styles.workoutDetailText}>{workoutITime}</Text>
                </View>
              </View>
            </View>
          </View>
          <FlatList
            horizontal
            scrollEnabled
            showsHorizontalScrollIndicator={false}
            scrollEventThrottle={16}
            data={workoutItems}
            keyExtractor={item => item.imageUrl}
            renderItem={itemData => (
              // <View>
              //   <Text>{itemData.item.title}</Text>
              // </View>
              <View style={(styles.container, styles.workouts)}>
                {/* <TouchableWithoutFeedback
                  onPress={() => {
                    props.onSelect(
                      itemData.item.workouts,
                      itemData.item.imageUrl,
                      itemData.item.title,
                      itemData.item.sequences,
                      itemData.item.exercises,
                      itemData.item.time,
                    );
                  }}
                  useForeground> */}
                <View style={{height: '100%', width: '100%'}}>
                  <View style={styles.imageViewContiner}>
                    <Image
                      style={styles.imageContainer}
                      source={{uri: itemData.item.imageUrl}}
                    />
                  </View>
                  <View style={styles.container}>
                    <View
                      style={{
                        flex: 1,
                        justifyContent: 'center',
                        paddingLeft: 20,
                      }}>
                      <Text
                        style={{
                          color: '#FF1654',
                          fontSize: 17,
                          fontWeight: 'bold',
                        }}>
                        {itemData.item.title}
                      </Text>
                      <View style={styles.headerDetailContainer}>
                        <Text style={styles.workoutDetailText}>
                          {itemData.item.laps} Sequences
                        </Text>
                        <Text style={styles.workoutDetailText}> - </Text>
                        <Text style={styles.workoutDetailText}>
                          {itemData.item.time} mins
                        </Text>
                      </View>
                    </View>
                  </View>
                </View>
                {/* </TouchableWithoutFeedback> */}
              </View>
            )}
          />
        </View>
      </Animated.ScrollView>
      <TouchableOpacity onPress={() => NavigationService.navigate('Workouts')}>
        <View style={styles.bottomBarContainer}>
          <View>
            <Text
              style={{
                color: 'white',
                fontWeight: 'bold',
                fontSize: 18,
                paddingBottom: 30,
              }}>
              Start Workout
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
};

WorkoutDetailScreen.navigationOptions = navData => {
  return {
    header: null,
  };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  centered: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: '100%',
    height: 300,
  },
  actions: {
    marginVertical: 10,
    alignItems: 'center',
  },
  price: {
    fontSize: 20,
    color: '#888',
    textAlign: 'center',
    marginVertical: 20,
  },
  description: {
    fontSize: 14,
    textAlign: 'center',
    marginHorizontal: 20,
  },
  imageViewContiner: {
    height: '70%',
    overflow: 'hidden',
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
  },
  headerImageStyle: {
    height: 300,
    width: '100%',
    alignSelf: 'center',
    position: 'absolute',
  },
  topBarContainer: {
    width: '100%',
    height: 85,
    position: 'absolute',
    justifyContent: 'center',
    zIndex: 100,
  },
  bottomBarContainer: {
    width: '100%',
    height: 75,
    bottom: 0,
    backgroundColor: '#FF1654',
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  headerText: {
    fontSize: 28,
    fontWeight: 'bold',
  },
  headerDetailContainer: {
    paddingTop: 3,
    flexDirection: 'row',
  },

  workoutDetailText: {
    fontSize: 16,
    color: 'gray',
  },
  workoutDetailText: {
    fontSize: 12,
    color: 'gray',
  },
  workouts: {
    width: width - 36 * 3,
    height: width - 36 * 4,
    marginLeft: 20,
    marginVertical: 20,
    borderRadius: 5,
    backgroundColor: 'white',
    borderColor: 'lightgray',
    borderWidth: 0.5,
    shadowOpacity: 0.2,
    shadowRadius: 10,
    shadowColor: 'black',
    shadowOffset: {height: 3, width: 0},
    elevation: 10,
  },
  workoutsSecond: {
    flex: 1,
    alignItems: 'stretch',
    height: 80,
    marginHorizontal: 20,
    marginVertical: 8,
    borderRadius: 5,
    backgroundColor: 'white',
    borderColor: 'lightgray',
    borderWidth: 0.5,
    shadowOpacity: 0.2,
    shadowRadius: 10,
    shadowColor: 'black',
    shadowOffset: {height: 3, width: 0},
    elevation: 10,
  },
  imageViewContiner: {
    height: '70%',
    overflow: 'hidden',
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
  },
  imageContainer: {
    width: '100%',
    height: '100%',
  },
});

export default WorkoutDetailScreen;
